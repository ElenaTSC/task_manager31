package ru.tsk.ilina.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists");
    }

    public LoginExistsException(@NotNull final String message) {
        super("Error! " + message + " login already exists");
    }

}
