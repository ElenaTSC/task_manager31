package ru.tsk.ilina.tm.exception.entity;

import ru.tsk.ilina.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found");
    }

}
