package ru.tsk.ilina.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.ICommandRepository;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.model.Command;

import java.util.*;

@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getCommand() {
        return commands.values();
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandName() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : commands.values()) {
            @NotNull final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArg() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand argument : arguments.values()) {
            @NotNull final String arg = argument.arg();
            if (arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @NotNull
    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
