package ru.tsk.ilina.tm.command.backup;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.command.data.AbstractDataCommand;
import ru.tsk.ilina.tm.dto.Domain;

import java.io.FileOutputStream;

public class BackupSaveCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "backup-save";
    }

    @Override
    public @Nullable String description() {
        return null;
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
