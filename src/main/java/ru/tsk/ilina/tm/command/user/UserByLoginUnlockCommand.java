package ru.tsk.ilina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class UserByLoginUnlockCommand extends AbstractAuthUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("[ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
