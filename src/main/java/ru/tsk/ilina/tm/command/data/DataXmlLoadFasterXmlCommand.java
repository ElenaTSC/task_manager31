package ru.tsk.ilina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-xml-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Load data in XML with FasterXml library";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTER_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
