package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().finishByID(userId, id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
