package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;

public final class ShowCommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands.";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (@NotNull final String command : serviceLocator.getCommandService().getListCommandName()) {
            System.out.println(command);
        }
    }

}
