package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class ProjectTaskRemoveAllByIdCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "project_task_remove_all_by_id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all task bind project id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
