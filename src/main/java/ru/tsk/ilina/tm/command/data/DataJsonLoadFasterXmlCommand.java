package ru.tsk.ilina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @NotNull String name() {
        return "data-load-json-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Load data in JSON with FasterXml library";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTER_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}
