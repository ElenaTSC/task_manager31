package ru.tsk.ilina.tm.command.backup;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.command.data.AbstractDataCommand;
import ru.tsk.ilina.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "backup-load";
    }

    @Override
    public @Nullable String description() {
        return null;
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @SneakyThrows
    @Override
    public @NotNull void execute() {
        if (new File(BACKUP_JSON).exists()) {
            @NotNull final String json = new String(Files.readAllBytes(Paths.get(BACKUP_JSON)));
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        }
    }
}
