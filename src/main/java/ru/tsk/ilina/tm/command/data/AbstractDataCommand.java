package ru.tsk.ilina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.dto.Domain;
import ru.tsk.ilina.tm.exception.empty.EmptyDomainException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";
    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";
    @NotNull
    protected static final String FILE_JAXB_XML = "./data_jaxb.xml";
    @NotNull
    protected static final String FILE_FASTER_XML = "./data_faster.xml";
    @NotNull
    protected static final String FILE_JAXB_JSON = "./data_jaxb.json";
    @NotNull
    protected static final String FILE_FASTER_JSON = "./data_faster.json";
    @NotNull
    protected static final String FILE_YAM = "./data.yaml";
    @NotNull
    protected static final String JAXB_PROPERTY_NAME = "eclipselink.media-type";
    @NotNull
    protected static final String JAXB_PROPERTY_VALUE = "application/json";
    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";
    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    protected static final String BACKUP_JSON = "./backup.json";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }
}
