package ru.tsk.ilina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class ProjectTaskUnbindByIdCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "project_task_unbind_by_id";
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task by project id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER PROJECT ID]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findByID(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findByID(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final Task taskUpdate = serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
