package ru.tsk.ilina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsk.ilina.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application";
    }

    @NotNull
    @Override
    public String arg() {
        return "-e";
    }

    @NotNull
    @Override
    public void execute() {
        System.exit(0);
    }

}
