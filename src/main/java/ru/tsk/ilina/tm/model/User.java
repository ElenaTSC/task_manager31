package ru.tsk.ilina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.util.HashUtil;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    @Nullable
    private boolean locked = false;

}
