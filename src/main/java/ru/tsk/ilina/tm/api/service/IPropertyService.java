package ru.tsk.ilina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getEmail();

    @NotNull
    String getDeveloper();

}
