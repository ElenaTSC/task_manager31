package ru.tsk.ilina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.ilina.tm.api.repository.IUserRepository;
import ru.tsk.ilina.tm.api.service.IPropertyService;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.empty.EmptyEmailException;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyLoginException;
import ru.tsk.ilina.tm.exception.empty.EmptyPasswordException;
import ru.tsk.ilina.tm.exception.user.LoginExistsException;
import ru.tsk.ilina.tm.model.User;
import ru.tsk.ilina.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IUserRepository userRepository;
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final PropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public User addUser(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.add(user);
    }

    @Override
    public User addUser(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        final User user = addUser(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User addUser(@NotNull final String login, @NotNull final String password, @Nullable final Role role) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (role == null) return null;
        final User user = addUser(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        return repository.findByEmail(email);
    }

    @Override
    public User setPassword(@NotNull final String userId, @NotNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(@NotNull final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName
    ) {
        if (userId.isEmpty()) throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return repository.removeByLogin(login);
    }

    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return findByLogin(login) != null;
    }

    @Override
    public User lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = repository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final User user = repository.findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }
}
