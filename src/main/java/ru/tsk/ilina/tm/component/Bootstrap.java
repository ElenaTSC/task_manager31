package ru.tsk.ilina.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsk.ilina.tm.api.service.*;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.repository.CommandRepository;
import ru.tsk.ilina.tm.repository.ProjectRepository;
import ru.tsk.ilina.tm.repository.TaskRepository;
import ru.tsk.ilina.tm.repository.UserRepository;
import ru.tsk.ilina.tm.service.*;
import ru.tsk.ilina.tm.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;

import static ru.tsk.ilina.tm.util.SystemUtil.getPID;

@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final PropertyService propertyService = new PropertyService();
    @NotNull
    private final UserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final CommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    public void start(@NotNull String[] args) {
        logService.debug("Test environment");
        System.out.println("**WELCOME TO TASK MANAGER**");
        if (runArg(args)) System.exit(0);
        @Nullable String command = "";
        initUsers();
        initData();
        initPid();
        initCommands();
        backup.init();
        fileScanner.init();
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND: ");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
        fileScanner.stop();
    }

    @NotNull
    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsk.ilina.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsk.ilina.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registry(clazz.newInstance());
        }
    }

    @NotNull
    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @NotNull
    public void runCommand(@NotNull final String command) {
        if (command.isEmpty()) return;
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        @NotNull final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    @NotNull
    private boolean runArg(@NotNull final String[] args) {
        if (args.length == 0) return false;
        @NotNull AbstractCommand abstractCommand = commandService.getCommandByArg(args[0]);
        abstractCommand.execute();
        return true;
    }

    @NotNull
    private void initData() {
        @NotNull Project project1 = new Project();
        project1.setName("Project C");
        project1.setStatus(Status.COMPLETED);
        project1.setStartDate(new Date());
        String userId = "test";
        projectService.add(userId, project1);
        @NotNull Project project2 = new Project();
        project2.setName("Project A");
        project2.setStatus(Status.IN_PROGRESS);
        project1.setStartDate(new Date());
        projectService.add(userId, project2);
        @NotNull Project project3 = new Project();
        project3.setName("Project B");
        projectService.add(userId, project3);
        @NotNull Task task1 = new Task();
        task1.setName("Task A");
        task1.setStatus(Status.COMPLETED);
        taskService.add(userId, task1);
        @NotNull Task task2 = new Task();
        task2.setName("Task B");
        task2.setStatus(Status.COMPLETED);
        taskService.add(userId, task2);
        @NotNull Task task3 = new Task();
        task3.setName("Task C");
        task3.setStatus(Status.COMPLETED);
        taskService.add(userId, task3);
    }

    @NotNull
    private void initUsers() {
        userService.addUser("test", "test", "test@test.com");
        userService.addUser("admin", "admin", Role.ADMIN);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
